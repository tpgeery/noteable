#!/bin/bash

echo "Starting CI"

RES1="$( python main.py -t A | head -n 3 | tail -n 1 )"
RES2="$( python main.py -t A | tail -n 3 | head -n 1 )"
RES3="$( python main.py -t A | tail -n 2 | head -n 1 )"
RES4="$( python main.py -t A | tail -n 1 | head -n 1 )"
RES=0

if [[ $RES1 == *"A C# E"* ]]; then
	echo "Test 1: Pass"
else
	echo "Test 1: Fail"
	RES=1
fi

if [[ "$RES2" == *"A C E"* ]]; then
	echo "Test 2: Pass"
else
	echo "Test 2: Fail"
	RES=1
fi

if [[ "$RES3" == *"A C# F"* ]]; then
	echo "Test 3: Pass"
else
	echo "Test 3: Fail"
	RES=1
fi

if [[ "$RES4" == *"A C D#"* ]]; then
	echo "Test 4: Pass"
else
	echo "Test 4: Fail"
	RES=1
fi

if [ $RES == 1 ]; then
	exit 1
fi
